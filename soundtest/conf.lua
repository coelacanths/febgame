function love.conf(t)
  -- set supported löve version
  t.version = "0.10.0"

  -- set window options
  t.window.title = "love2d sound test"
  t.window.width = 320
  t.window.height = 240

  -- disable unneeded modules
  t.modules.image = false
  t.modules.joystick = false
  t.modules.keyboard = false
  t.modules.math = false
  t.modules.mouse = false
  t.modules.physics = false
  t.modules.system = false
  t.modules.thread = false
  t.modules.timer = false
  t.modules.touch = false
  t.modules.video = false
end

FILENAME = "music.it"

function love.load()
  -- init font
  font = love.graphics.newFont(24)
  love.graphics.setFont(font)

  -- attempt to play IT module
  if love.filesystem.exists(FILENAME) then
    music = love.audio.newSource(FILENAME)
    music:setLooping(true)
    music:play()
  end
end

function love.draw()
  -- format status text
  local text
  if music then
    pos = music:tell()
    text = string.format("playing - %d:%02d", pos / 60, pos % 60)
  else
    text = "no " .. FILENAME .. " :("
  end

  -- draw status text
  width, height = love.window.getMode()
  x, y = width / 2, (height - font:getHeight()) / 2
  love.graphics.printf(text, x - width / 2, y, width, "center")
end

love2d sound test
-----------------

this "game" uses löve to play an IT module in order to verify that IT playback
is working as desired.

in order to test a module, place it in this directory as `music.it`, then [run
using löve](https://love2d.org/wiki/Getting_Started#Running_Games).

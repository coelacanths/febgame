love2d font test
----------------

this "game" uses löve to render an image font for testing.

in order to test a font, place it in this directory as `font.png`, then [run
using löve](https://love2d.org/wiki/Getting_Started#Running_Games). the image
must use löve's [image font format](https://love2d.org/wiki/ImageFontFormat),
an example of which can be found
[here](https://love2d.org/w/images/6/67/Resource-Imagefont.png).

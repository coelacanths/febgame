FILENAME = "font.png"
GLYPHS =
  " abcdefghijklmnopqrstuvwxyz" ..
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
  "123456789.,!?-+/():;%&`'*#=[]\""

function love.load()
  -- attempt to load font
  local font
  if love.filesystem.exists(FILENAME) then
    font = love.graphics.newImageFont(FILENAME, GLYPHS)
    love.graphics.setFont(font)
    body = "type to enter text: "
  else
    body = "no " .. FILENAME .. " :("
  end

  -- init unscaled buffer canvas
  love.graphics.setDefaultFilter("nearest", "nearest")
  width, height = love.window.getMode()
  canvas = love.graphics.newCanvas(width / 2, height / 2)

  -- set key repeat
  love.keyboard.setKeyRepeat(true)
end

function love.draw()
  -- draw text to canvas
  width, _ = canvas:getDimensions()
  love.graphics.setCanvas(canvas)
  love.graphics.clear()
  love.graphics.printf(body, 0, 0, width, "left")

  -- draw canvas to screen
  love.graphics.setCanvas()
  love.graphics.draw(canvas, 0, 0, 0, 2, 2)
end

function love.textinput(text)
  if #text == 1 then
    body = body .. text
  end
end

function love.keypressed(key)
  if key == "backspace" then
    body = string.sub(body, 1, #body - 1)
  end
end
